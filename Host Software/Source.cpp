﻿/*
 *	Host-side software which capture screen pixel colors in specific areas
 * calculates its average color saving in array which is later send to arduino, ready to lit leds on ledstrip in specific color.
 * Based on Adalight Device and color capture software like Prismatik or boblight.
 * "Magic Word" for synchronisation is 'Ada' followed by LED High, Low and Checksum
 * @author: Adam Drabik "Advem" <advem@vp.pl>
 * @date: 11.06.2019
 */

// Jeżeli masz problem z uruchomieniem aplikacji (przez biblioteke allegro) za pomocją Visual Studio otwórz plik AmbientLight.vcxproj 
// wejdz w Projekt(Project) /  AmbientLight Właściwości... (AmbientLight Properties...)
// ustaw konfiguracje na Debug, pod zakłądką Allegro5/Add-ons ustaw Primitive Addons na Yes
// pod zakładką Library Type - ustaw Library Type na Dynamic Debug

#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <fstream>
#include <Windows.h>
#include <allegro5/allegro.h> // biblioteka graficzna do wizualizacji
#include <allegro5/allegro_primitives.h> // dodatek do wizualizacji pixeli
#include "SerialPort.h" // biblioteka obsługująca połączenie usb 

using namespace std;

// nie można pobrać rozdzielczości automatycznie, gdyż w przypadku korzystania z skalowania interfejsu aplikacji np. 175%, 
// odczytywana wartość GetSystemMetrics to właściwaRozdzielczość / 1.75. nalezy wiec ustawic rozdzielczosc recznie
//int nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
//int nScreenHeight = GetSystemMetrics(SM_CYSCREEN);

uint16_t nScreenWidth = 1920; // bazowa szerokość ekranu
uint16_t nScreenHeight = 1080; // bazowa wysokość ekranu
uint16_t scale = 2; // zmniejsz okno wizualizatora
const uint16_t numLeds = 98; // liczba diod led na taśmie
uint8_t top_bot_Leds = 31; // ilość diod na górze i dole ekranu
uint8_t left_right_Leds = 18; // ilość diod po bokach ekranu
uint8_t offset = 18; // ustaw początek wyświetlania na diodę
uint8_t zoneWidth = nScreenWidth / top_bot_Leds; // wymiar x  obszaru próbkowania
uint8_t zoneHeight = nScreenHeight / left_right_Leds; // wymiar y obszaru próbkowania
uint8_t pixelR, pixelG, pixelB; // zmienna uzywane do funkcji show...
double totalR, totalG, totalB; // zmienna przechowująca sume wartości RGB potrzebne do obliczenia średniej

char port[] = "COM5";  // nazwa portu usb arduino

const float FPS = 60; // odświeżanie wizualizacji

void showResolution() {
	int nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int nScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	cout << "Windows Resolution: " << nScreenWidth << " x " << nScreenHeight << endl;
}
void showAllPixels() {

	HWND desktop = GetDesktopWindow();
	HDC desktopHdc = GetDC(desktop);

	for (int y = 0; y < nScreenHeight; y++) {
		for (int x = 0; x < nScreenWidth; x++) {
			COLORREF color = GetPixel(desktopHdc, x, y);
			pixelR = GetRValue(color);
			pixelG = GetGValue(color);
			pixelB = GetBValue(color);
			cout << "Pixel color in x:" << x << " y:" << y << " = " << "rgb(" << pixelR << ", " << pixelG << ", " << pixelB << ")" << endl;
		}
	}
}
void showPixelColor(int x, int y) {
	while (1) {

		HWND desktop = GetDesktopWindow();
		HDC desktopHdc = GetDC(desktop);

		COLORREF color = GetPixel(desktopHdc, x, y);
		pixelR = GetRValue(color);
		pixelG = GetGValue(color);
		pixelB = GetBValue(color);
		cout << "Pixel color in x:" << x << " y:" << y << " = " << "rgb(" << pixelR << ", " << pixelG << ", " << pixelB << ")" << endl;
	}
}
void showAreaColor(int x1, int y1, int x2, int y2) {

	HWND desktop = GetDesktopWindow();
	HDC desktopHdc = GetDC(desktop);
	COLORREF color;

	int areaColor = 0;
	int areaR, areaG, areaB;
	int countPixels = (x2 - x1 + 1)*(y2 - y1 + 1);

	while (1) {
		for (int y = y1; y <= y2; y++) {
			for (int x = x1; x <= x2; x++) {
				color = GetPixel(desktopHdc, x, y);
				areaColor += color;
			}
		}

		areaR = GetRValue(areaColor / countPixels);
		areaG = GetGValue(areaColor / countPixels);
		areaB = GetBValue(areaColor / countPixels);

		cout << "Area from pixel(" << x1 << ", " << y1 << ") to pixel(" << x2 << ", " << y2 << ") of total " << countPixels << " pixels = rgb(" << areaR << ", " << areaG << ", " << areaB << ")" << endl;
	}
}
void CaptureScreen(int x1, int y1, int x2, int y2)
{
	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);

	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT);

	BITMAPINFO bmi = { 0 };
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = nScreenWidth;
	bmi.bmiHeader.biHeight = nScreenHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	RGBQUAD *pPixels = new RGBQUAD[nScreenWidth * nScreenHeight];

	GetDIBits(
		hCaptureDC,
		hCaptureBitmap,
		0,
		nScreenHeight,
		pPixels,
		&bmi,
		DIB_RGB_COLORS
	);

	int p = 0;
	int countPixels = (x2 - x1 + 1)*(y2 - y1 + 1);

	totalR = 0;
	totalG = 0;
	totalB = 0;

	for (int y = 0; y < nScreenHeight; y++) {
		for (int x = 0; x < nScreenWidth; x++) {
			p = (nScreenHeight - y - 1)*nScreenWidth + x; // upside down
			int R = pPixels[p].rgbRed;
			int G = pPixels[p].rgbGreen;
			int B = pPixels[p].rgbBlue;
			if (x >= x1 && x <= x2 && y >= y1 && y <= y2) { // suma kolorów pixeli w obszarze od p(x1,y1) do p(x2,y2)
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
				//cout << "Pixel p: " << p << " color in x:" << x << " y:" << y << " = " << "rgb(" << R << ", " << G << ", " << B << ")" << endl; // pokaz wszystkie pixele
			}
		}
	}

	// usrednienie kolorow pixeli w obszarze od p(x1,y1) do p(x2,y2)
	totalR /= countPixels;
	totalG /= countPixels;
	totalB /= countPixels;

	cout << "Area from pixel(" << x1 << ", " << y1 << ") to pixel(" << x2 << ", " << y2 << ") of total " << countPixels << " pixels = rgb(" << totalR << ", " << totalG << ", " << totalB << ")" << endl; // pokaz pixele w wybranym obszarze od p(x1,y1) do p(x2,y2)

	delete[] pPixels;
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);
}
vector<int> CapturePixelColor(int px, int py) {
	vector<int> PixelColor(3);

	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);

	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT);

	BITMAPINFO bmi = { 0 };
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = nScreenWidth;
	bmi.bmiHeader.biHeight = nScreenHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	RGBQUAD *pPixels = new RGBQUAD[nScreenWidth * nScreenHeight];

	GetDIBits(
		hCaptureDC,
		hCaptureBitmap,
		0,
		nScreenHeight,
		pPixels,
		&bmi,
		DIB_RGB_COLORS
	);

	int p = 0;

	for (int y = 0; y < nScreenHeight; y++) {
		for (int x = 0; x < nScreenWidth; x++) {
			p = (nScreenHeight - y - 1)*nScreenWidth + x; // upside down
			if (x == px && y == py) { // wskazany pixel o współrzędnych px, py
				PixelColor[0] = pPixels[p].rgbRed;
				PixelColor[1] += pPixels[p].rgbGreen;
				PixelColor[2] += pPixels[p].rgbBlue;
				//cout << "Pixel p: " << p << " color in x:" << x << " y:" << y << " = " << "rgb(" << R << ", " << G << ", " << B << ")" << endl; // pokaz wszystkie pixele
			}
		}
	}

	delete[] pPixels;
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);

	return PixelColor;
}

// Dla całego ekranu -> utwórz bitmape całego ekranu -> zwróć do wektora 3 wartości (R,G,B) gdy ten jest w obszarze -> pokoloruj obszar		 (98 wektorów po 3 wartości | 1 bitmapa ekranu wykonywana 98 razy) 
vector<int> CaptureAreaColor_UnOptimalized(int x1, int y1, int x2, int y2) {

	vector<int> color(3);

	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);

	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT);

	BITMAPINFO bmi = { 0 };
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = nScreenWidth;
	bmi.bmiHeader.biHeight = nScreenHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	RGBQUAD *pPixels = new RGBQUAD[nScreenWidth * nScreenHeight];

	GetDIBits(
		hCaptureDC,
		hCaptureBitmap,
		0,
		nScreenHeight,
		pPixels,
		&bmi,
		DIB_RGB_COLORS
	);

	int p = 0;
	int countPixels = (x2 - x1 + 1)*(y2 - y1 + 1);

	totalR = 0;
	totalG = 0;
	totalB = 0;

	for (int y = 0; y < nScreenHeight; y++) {
		for (int x = 0; x < nScreenWidth; x++) {
			p = (nScreenHeight - y - 1)*nScreenWidth + x; // upside down
			if (x >= x1 && x <= x2 && y >= y1 && y <= y2) { // suma kolorów pixeli w obszarze od p(x1,y1) do p(x2,y2)
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
				//cout << "Pixel p: " << p << " color in x:" << x << " y:" << y << " = " << "rgb(" << R << ", " << G << ", " << B << ")" << endl; // pokaz wszystkie pixele w obszarze
			}
		}
	}

	// usrednienie kolorow pixeli w obszarze od p(x1,y1) do p(x2,y2) i przypisanie do wektora vector<int>color
	color[0] = totalR / countPixels;
	color[1] = totalG / countPixels;
	color[2] = totalB / countPixels;

	//cout << "Area from pixel(" << x1 << ", " << y1 << ") to pixel(" << x2 << ", " << y2 << ") of total " << countPixels << " pixels = rgb(" << totalR << ", " << totalG << ", " << totalB << ")" << endl; // pokaz pixele w wybranym obszarze od p(x1,y1) do p(x2,y2)

	delete[] pPixels;
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);

	return color;
}
void setZone() {
	vector<int> color = CapturePixelColor(0, 0);

	// Led 1 - 31
	for (int i = 1; i <= top_bot_Leds; i++)
		for (int x = zoneWidth * (i - 1) + 1; x <= zoneWidth * i; x++)
			for (int y = 0; y <= zoneHeight; y++) {
				al_draw_filled_rectangle(x / 2, y / 2, (x + 1) / 2, (y + 1) / 2, al_map_rgb(color[0], color[1], color[2]));
				//cout << "led: " << i << " " << x << "x" << y << endl; // led 1 -> led 31
			}

	// Led 32 - 49
	for (int i = 1; i <= left_right_Leds; i++)
		for (int y = zoneHeight * (i - 1) + 1; y <= zoneHeight * i; y++)
			for (int x = nScreenWidth - zoneWidth; x <= nScreenWidth; x++) {
				al_draw_filled_rectangle(x / 2, y / 2, (x + 1) / 2, (y + 1) / 2, al_map_rgb(color[0], color[1], color[2]));
				//cout << "led: " << i + top_bot_Leds << " " << x << "x" << y << endl; // led 1 + 31 -> led 18 + 31
			}

	// Led 50 - 80
	for (int i = 1; i <= top_bot_Leds; i++)
		for (int x = nScreenWidth - zoneWidth * (i - 1); x >= nScreenWidth - zoneWidth * i; x--)
			for (int y = nScreenHeight - zoneHeight; y <= nScreenHeight; y++) {
				al_draw_filled_rectangle(x / 2, y / 2, (x + 1) / 2, (y + 1) / 2, al_map_rgb(color[0], color[1], color[2]));
				//cout << "led: " << i + top_bot_Leds + left_right_Leds << " " << x << "x" << y << endl; // led 1 + 31 + 18 -> led 31 + 31 + 18
			}

	// Led 81 - 98
	for (int i = 1; i <= left_right_Leds; i++)
		for (int y = nScreenHeight - zoneHeight * i; y <= nScreenHeight - zoneHeight * (i - 1); y++)
			for (int x = 0; x <= zoneWidth; x++) {
				al_draw_filled_rectangle(x / 2, y / 2, (x + 1) / 2, (y + 1) / 2, al_map_rgb(color[0], color[1], color[2]));
				//cout << "led: " << i + top_bot_Leds + left_right_Leds + top_bot_Leds << " " << x << "x" << y << endl; // led 1 + 31 + 18 + 31 -> led 18 + 31 + 18 + 31
			}
}

// Dla każdego obszaru -> utwórz bitmape tylko tego obszaru -> zwróć do vectora 3 wartości (R,G,B) -> pokoloruj obszar			(98 wektorów po 3 wartości | 98  bitmap kawałka ekranu po 1 razie)
vector<uint16_t> CaptureAreaColorOptim(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2) {

	vector<uint16_t> color(3);
	int p = 0;
	uint16_t pixelsWidth = (x2 - x1 + 1);
	uint16_t pixelsHeight = (y2 - y1 + 1);
	int countPixels = pixelsWidth * pixelsHeight;

	totalR = 0;
	totalG = 0;
	totalB = 0;

	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);

	BitBlt(hCaptureDC, 0, 0, pixelsWidth, pixelsHeight, hDesktopDC, x1, y1, SRCCOPY | CAPTUREBLT);

	BITMAPINFO bmi = { 0 };
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = pixelsWidth;
	bmi.bmiHeader.biHeight = pixelsHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	RGBQUAD *pPixels = new RGBQUAD[countPixels];

	GetDIBits(
		hCaptureDC,
		hCaptureBitmap,
		0,
		pixelsHeight,
		pPixels,
		&bmi,
		DIB_RGB_COLORS
	);

	for (int y = 0; y < pixelsHeight; y++) {
		for (int x = 0; x < pixelsWidth; x++) {
			p = (pixelsHeight - y - 1)*pixelsWidth + x; // upside down
			 // suma kolorów pixeli w obszarze od p(x1,y1) do p(x2,y2)
			totalR += pPixels[p].rgbRed;
			totalG += pPixels[p].rgbGreen;
			totalB += pPixels[p].rgbBlue;
			//cout << "Pixel p: " << p << " color in x:" << x << " y:" << y << " = " << "rgb(" << (int)pPixels[p].rgbRed << ", " << (int)pPixels[p].rgbGreen << ", " << (int)pPixels[p].rgbBlue<< ")" << endl; // pokaz wszystkie pixele w obszarze
		}
	}

	// usrednienie kolorow pixeli w obszarze od p(x1,y1) do p(x2,y2) i przypisanie do wektora vector<int>color
	color[0] = totalR / countPixels;
	color[1] = totalG / countPixels;
	color[2] = totalB / countPixels;

	//cout << "Area from pixel(" << x1 << ", " << y1 << ") to pixel(" << x2 << ", " << y2 << ") of total " << countPixels << " pixels = rgb(" << color[0] << ", " << color[1] << ", " << color[2] << ")" << endl; // pokaz pixele w wybranym obszarze od p(x1,y1) do p(x2,y2)

	delete[] pPixels;
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);

	return color;
}
void setZoneOptim() {

	uint8_t led = 1;

	//while(led <= numLeds) { // loop przez wszystkie ledy od 1 do 98 // removed
	if (led <= top_bot_Leds) { // ledy góra
		for (uint8_t i = 1; i <= top_bot_Leds; i++) { // poszczególny led od 1 do 31 numeracja od 1 do ilość ledów na tym boku
			vector<uint16_t> color = CaptureAreaColorOptim(zoneWidth * (i - 1), 0, zoneWidth * i, zoneHeight); // pobierz uśredniony kolor obszaru
			al_draw_filled_rectangle((zoneWidth * (i - 1)) / scale, 0 / scale, (zoneWidth * i) / scale, zoneHeight / scale, al_map_rgb(color[0], color[1], color[2])); // dzielmy współrzędne na 2 bo ekran wizualizacji jest 2 razy mniejszy
			//cout << "Led #" << led << " = rgb(" << color[0] << ", " << color[1] << ", " << color[2] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds && led <= top_bot_Leds + left_right_Leds) { // ledy prawo
		for (uint8_t i = 1; i <= left_right_Leds; i++) { // led od 32 do 49
			vector<uint16_t> color = CaptureAreaColorOptim(nScreenWidth - zoneWidth, zoneHeight * (i - 1), nScreenWidth, zoneHeight * i);
			al_draw_filled_rectangle((nScreenWidth - zoneWidth) / scale, (zoneHeight * (i - 1)) / scale, (nScreenWidth) / scale, (zoneHeight * i) / scale, al_map_rgb(color[0], color[1], color[2]));
			//cout << "Led #" << led << " = rgb(" << color[0] << ", " << color[1] << ", " << color[2] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds + left_right_Leds && led <= top_bot_Leds + left_right_Leds + top_bot_Leds) { // ledy dół
		for (uint8_t i = 1; i <= top_bot_Leds; i++) { // led od 50 do 80
			vector<uint16_t> color = CaptureAreaColorOptim(nScreenWidth - (zoneWidth * i), nScreenHeight - zoneHeight, nScreenWidth - (zoneWidth * (i - 1)), nScreenHeight);
			al_draw_filled_rectangle((nScreenWidth - (zoneWidth * i)) / scale, (nScreenHeight - zoneHeight) / scale, (nScreenWidth - (zoneWidth * (i - 1))) / scale, (nScreenHeight) / scale, al_map_rgb(color[0], color[1], color[2]));
			//cout << "Led #" << led << " = rgb(" << color[0] << ", " << color[1] << ", " << color[2] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds + left_right_Leds + top_bot_Leds && led <= top_bot_Leds + left_right_Leds + top_bot_Leds + left_right_Leds) { // ledy lewo
		for (uint8_t i = 1; i <= left_right_Leds; i++) { // led od 81 do 98
			vector<uint16_t> color = CaptureAreaColorOptim(0, nScreenHeight - (zoneHeight * i), zoneWidth, nScreenHeight - (zoneHeight * (i - 1)));
			al_draw_filled_rectangle((0) / scale, (nScreenHeight - (zoneHeight * i)) / scale, (zoneWidth) / scale, (nScreenHeight - (zoneHeight * (i - 1))) / scale, al_map_rgb(color[0], color[1], color[2]));
			//cout << "Led #" << led << " = rgb(" << color[0] << ", " << color[1] << ", " << color[2] << ")" << endl;
			led++;
		}
	}

	//} // removed
}

// Dla całego ekranu - >utwórz bitmape całego ekranu -> oblicz wartości RGB na 98 obszarach -> zwróć do vectora wartości RGB 98 obszarów -> pokoloruj obszary wartościami z wektora			(1 wektor po 3x98 wartości |  1 bitmapa ekranu wykonana 1 raz)
vector<uint16_t> CaptureAreaColorTurboEdition() {

	vector<uint16_t> colors(3 * numLeds);
	int p = 0;
	int countPixels = nScreenWidth * nScreenHeight;
	totalR = 0; totalG = 0; totalB = 0;

	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);

	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT);

	BITMAPINFO bmi = { 0 };
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = nScreenWidth;
	bmi.bmiHeader.biHeight = nScreenHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	RGBQUAD *pPixels = new RGBQUAD[countPixels];

	GetDIBits(hCaptureDC, hCaptureBitmap, 0, nScreenHeight, pPixels, &bmi, DIB_RGB_COLORS);

	// obszar górny
	for (int i = 1; i <= top_bot_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = zoneWidth * (i - 1); x < zoneWidth * i; x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = 0; y < zoneHeight; y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 1 do 31
		colors[3 * i - 3] = totalR / ((zoneWidth * zoneHeight) - 1);
		colors[3 * i - 2] = totalG / ((zoneWidth * zoneHeight) - 1);
		colors[3 * i - 1] = totalB / ((zoneWidth * zoneHeight) - 1);
		totalR = 0; totalG = 0; totalB = 0;
	}
	// obszar prawy
	for (int i = 1; i <= left_right_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = nScreenWidth - zoneWidth; x <= nScreenWidth; x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = zoneHeight * (i - 1); y < zoneHeight * i; y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				//cout << "led: " << i+31 << " p: " << p << " x: " << x << " y: " << y << endl;
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 32 do 49
		colors[3 * (i + top_bot_Leds) - 3] = totalR / ((zoneWidth * zoneHeight) - 1);
		colors[3 * (i + top_bot_Leds) - 2] = totalG / ((zoneWidth * zoneHeight) - 1);
		colors[3 * (i + top_bot_Leds) - 1] = totalB / ((zoneWidth * zoneHeight) - 1);
		totalR = 0; totalG = 0; totalB = 0;
	}
	// obszar dolny
	for (int i = 1; i <= top_bot_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = nScreenWidth - (zoneWidth * i); x <= nScreenWidth - (zoneWidth * (i - 1)); x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = nScreenHeight - zoneHeight; y < nScreenHeight; y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 50 do 80
		colors[3 * (i + top_bot_Leds + left_right_Leds) - 3] = totalR / ((zoneWidth * zoneHeight) - 1);
		colors[3 * (i + top_bot_Leds + left_right_Leds) - 2] = totalG / ((zoneWidth * zoneHeight) - 1);
		colors[3 * (i + top_bot_Leds + left_right_Leds) - 1] = totalB / ((zoneWidth * zoneHeight) - 1);
		totalR = 0; totalG = 0; totalB = 0;
	}
	// obszar lewo
	for (int i = 1; i <= left_right_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = 0; x <= zoneWidth; x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = nScreenHeight - (zoneHeight * i); y < nScreenHeight - (zoneHeight * (i - 1)); y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 81 do 98
		colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 3] = totalR / ((zoneWidth * zoneHeight) - 1);
		colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 2] = totalG / ((zoneWidth * zoneHeight) - 1);
		colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 1] = totalB / ((zoneWidth * zoneHeight) - 1);
		totalR = 0; totalG = 0; totalB = 0;
	}

	delete[] pPixels;
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);

	return colors;
}
void setZoneTuroEdition() {

	int led = 1;
	vector<uint16_t> colors = CaptureAreaColorTurboEdition(); // pobierz array[3x98] RGB uśrednionych kolorów

	//for (int i = 0; i <= colors.size() - 1; i++) cout << "i=" << i << " color " << colors[i] << endl;

	if (led >= 1 && led <= top_bot_Leds) { // ledy góra
		for (int i = 1; i <= top_bot_Leds; i++) { // poszczególny led od 1 do 31 numeracja od 1 do ilość ledów na tym boku
			al_draw_filled_rectangle((zoneWidth * (i - 1)) / scale, 0 / scale, (zoneWidth * i) / scale, zoneHeight / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1])); // dzielmy współrzędne na scale bo ekran wizualizacji jest scale razy mniejszy
			// cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds && led <= top_bot_Leds + left_right_Leds) { // ledy prawo
		for (int i = 1; i <= left_right_Leds; i++) { // led od 32 do 49
			al_draw_filled_rectangle((nScreenWidth - zoneWidth) / scale, (zoneHeight * (i - 1)) / scale, (nScreenWidth) / scale, (zoneHeight * i) / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1]));
			//cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds + left_right_Leds && led <= top_bot_Leds + left_right_Leds + top_bot_Leds) { // ledy dół
		for (int i = 1; i <= top_bot_Leds; i++) { // led od 50 do 80
			al_draw_filled_rectangle((nScreenWidth - (zoneWidth * i)) / scale, (nScreenHeight - zoneHeight) / scale, (nScreenWidth - (zoneWidth * (i - 1))) / scale, (nScreenHeight) / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1]));
			//cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds + left_right_Leds + top_bot_Leds && led <= top_bot_Leds + left_right_Leds + top_bot_Leds + left_right_Leds) { // ledy lewo
		for (int i = 1; i <= left_right_Leds; i++) { // led od 81 do 98
			al_draw_filled_rectangle((0) / scale, (nScreenHeight - (zoneHeight * i)) / scale, (zoneWidth) / scale, (nScreenHeight - (zoneHeight * (i - 1))) / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1]));
			//cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}
}

// Dla całego ekranu - >utwórz bitmape całego ekranu -> oblicz wartości RGB na 98 obszarach -> zwróć do arraya wartości RGB 98 obszarów -> pokoloruj obszary wartościami z arraya			(1 array po 3x98 wartości |  1 bitmapa ekranu wykonana 1 raz)
uint8_t * AvgColors() {

	//vector<uint16_t> colors(3 * numLeds);
	static uint8_t colors[3 * numLeds];
	int p = 0;
	totalR = 0; totalG = 0; totalB = 0;

	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC, nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);

	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight, hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT);

	BITMAPINFO bmi = { 0 };
	bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);
	bmi.bmiHeader.biWidth = nScreenWidth;
	bmi.bmiHeader.biHeight = nScreenHeight;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;
	bmi.bmiHeader.biCompression = BI_RGB;

	RGBQUAD *pPixels = new RGBQUAD[nScreenWidth * nScreenHeight];

	GetDIBits(hCaptureDC, hCaptureBitmap, 0, nScreenHeight, pPixels, &bmi, DIB_RGB_COLORS);

	// obszar górny
	for (int i = 1; i <= top_bot_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = zoneWidth * (i - 1); x < zoneWidth * i; x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = 0; y < zoneHeight; y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 1 do 31
		colors[3 * i - 3] = totalR / ((zoneWidth * zoneHeight) - 0);
		colors[3 * i - 2] = totalG / ((zoneWidth * zoneHeight) - 0);
		colors[3 * i - 1] = totalB / ((zoneWidth * zoneHeight) - 0);
		//cout << "Led " << i << " rgb(" << colors[3 * i - 3] << ", " << colors[3 * i - 2]  << "," << colors[3 * i - 1] << ")" << endl;
		totalR = 0; totalG = 0; totalB = 0;
	}
	// obszar prawy
	for (int i = 1; i <= left_right_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = nScreenWidth - zoneWidth; x <= nScreenWidth; x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = zoneHeight * (i - 1); y < zoneHeight * i; y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				//cout << "led: " << i+31 << " p: " << p << " x: " << x << " y: " << y << endl;
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 32 do 49
		colors[3 * (i + top_bot_Leds) - 3] = totalR / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		colors[3 * (i + top_bot_Leds) - 2] = totalG / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		colors[3 * (i + top_bot_Leds) - 1] = totalB / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		//cout << "Led " << i + top_bot_Leds << " rgb(" << colors[3 * (i + top_bot_Leds) - 3] << ", " << colors[3 * (i + top_bot_Leds) - 2] << "," << colors[3 * (i + top_bot_Leds) - 1] << ")" << endl;
		totalR = 0; totalG = 0; totalB = 0;
	}
	// obszar dolny
	for (int i = 1; i <= top_bot_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = nScreenWidth - (zoneWidth * i); x <= nScreenWidth - (zoneWidth * (i - 1)); x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = nScreenHeight - zoneHeight; y < nScreenHeight; y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 50 do 80
		colors[3 * (i + top_bot_Leds + left_right_Leds) - 3] = totalR / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		colors[3 * (i + top_bot_Leds + left_right_Leds) - 2] = totalG / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		colors[3 * (i + top_bot_Leds + left_right_Leds) - 1] = totalB / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		//cout << "Led " << i + top_bot_Leds + left_right_Leds << " rgb(" << colors[3 * (i + top_bot_Leds + left_right_Leds) - 3] << ", " << colors[3 * (i + top_bot_Leds + left_right_Leds) - 2] << "," << colors[3 * (i + top_bot_Leds + left_right_Leds) - 1] << ")" << endl;
		totalR = 0; totalG = 0; totalB = 0;
	}
	// obszar lewo
	for (int i = 1; i <= left_right_Leds; i++) { // po kazdym ledzie w obszarze
		for (int x = 0; x <= zoneWidth; x++) { // po każdej współrzędnej x piksela w obszarze 
			for (int y = nScreenHeight - (zoneHeight * i); y < nScreenHeight - (zoneHeight * (i - 1)); y++) { // po każdej współrzędnej y piksela w obszarze 
				p = (nScreenHeight - y - 1)*nScreenWidth + x; // znajdz pixel na bitmapie
				totalR += pPixels[p].rgbRed;
				totalG += pPixels[p].rgbGreen;
				totalB += pPixels[p].rgbBlue;
			}
		} // led od 81 do 98z
		colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 3] = totalR / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 2] = totalG / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 1] = totalB / (((zoneWidth + 0) * (zoneHeight + 1)) + 0);
		//cout << "Led " << i + top_bot_Leds + left_right_Leds + top_bot_Leds << " rgb(" << colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 3] << ", " << colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 2] << "," << colors[3 * (i + top_bot_Leds + left_right_Leds + top_bot_Leds) - 1] << ")" << endl;
		totalR = 0; totalG = 0; totalB = 0;
	}

	delete[] pPixels;
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);

	return colors;
}
void displayColors() {

	int led = 1;
	uint8_t * colors = AvgColors(); // pobierz array[3x98] RGB uśrednionych kolorów

	//for (int i = 0; i <= colors.size() - 1; i++) cout << "i=" << i << " color " << colors[i] << endl;

	if (led >= 1 && led <= top_bot_Leds) { // ledy góra
		for (int i = 1; i <= top_bot_Leds; i++) { // poszczególny led od 1 do 31 numeracja od 1 do ilość ledów na tym boku
			al_draw_filled_rectangle((zoneWidth * (i - 1)) / scale, 0 / scale, (zoneWidth * i) / scale, zoneHeight / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1])); // dzielmy współrzędne na scale bo ekran wizualizacji jest scale razy mniejszy
			// cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds && led <= top_bot_Leds + left_right_Leds) { // ledy prawo
		for (int i = 1; i <= left_right_Leds; i++) { // led od 32 do 49
			al_draw_filled_rectangle((nScreenWidth - zoneWidth) / scale, (zoneHeight * (i - 1)) / scale, (nScreenWidth) / scale, (zoneHeight * i) / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1]));
			//cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds + left_right_Leds && led <= top_bot_Leds + left_right_Leds + top_bot_Leds) { // ledy dół
		for (int i = 1; i <= top_bot_Leds; i++) { // led od 50 do 80
			al_draw_filled_rectangle((nScreenWidth - (zoneWidth * i)) / scale, (nScreenHeight - zoneHeight) / scale, (nScreenWidth - (zoneWidth * (i - 1))) / scale, (nScreenHeight) / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1]));
			//cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}

	if (led > top_bot_Leds + left_right_Leds + top_bot_Leds && led <= top_bot_Leds + left_right_Leds + top_bot_Leds + left_right_Leds) { // ledy lewo
		for (int i = 1; i <= left_right_Leds; i++) { // led od 81 do 98
			al_draw_filled_rectangle((0) / scale, (nScreenHeight - (zoneHeight * i)) / scale, (zoneWidth) / scale, (nScreenHeight - (zoneHeight * (i - 1))) / scale, al_map_rgb(colors[3 * led - 3], colors[3 * led - 2], colors[3 * led - 1]));
			//cout << " Led # " << led << " rgb(" << colors[3 * led - 3] << ", " << colors[3 * led - 2] << ", " << colors[3 * led - 1] << ")" << endl;
			led++;
		}
	}
}

// Przesuń elemnty Array w celu rozpoczecia wyświetlania kolorów od diody offset
uint8_t * RotateArray(uint8_t Array[]) {
	static uint8_t Brray[3 * numLeds];
	for (int i = 0; i < 3 * numLeds; i++)
	Brray[(i + offset * 3) % (3 * numLeds)] = Array[i];
	return Brray;
}

int main() {

	// Inicjalizacja biblioteki graficznej
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_TIMER *timer = NULL;
	if (!al_init()) { fprintf(stderr, "failed to initialize allegro!\n"); return -1; }
	display = al_create_display(nScreenWidth / scale, nScreenHeight / scale); // rozpocznij ekran
	if (!display) { fprintf(stderr, "failed to create display!\n"); return -1; }
	al_init_primitives_addon(); // inicjowanie prymitywów
	timer = al_create_timer(1.0 / FPS);
	if (!timer) { fprintf(stderr, "failed to create timer!\n"); return -1; }
	al_start_timer(timer); // rozpocznij timer

	// Inicjalizacja połączenia z Arduino
	SerialPort arduino(port);
	if (arduino.isConnected()) { cout << "Connection made" << endl << endl; }
	else { cout << "Error in port name" << endl << endl; }

	// Inicjalizacja nagłówka informacji
	unsigned char  buffer[6 + (numLeds * 3)];
	buffer[0] = 'A';                          // Magic word
	buffer[1] = 'd';
	buffer[2] = 'a';
	buffer[3] = (numLeds - 1) >> 8;            // LED count high byte
	buffer[4] = (numLeds - 1) & 0xff;          // LED count low byte
	buffer[5] = buffer[3] ^ buffer[4] ^ 0x55; // Checksum

	// while arduino.isConnected()){}
	while (1) {
		// pokaż uśredniony kolor całego ekranu
				//vector<uint16_t> color = CaptureAreaColorOptim(x1,y1,x2,y2);
				//al_clear_to_color(al_map_rgb(color[0], color[1], color[2]));
				//cout << "Average Screen Color = rgb(" << color[0] << ", " << color[1] << ", " << color[2] << ")" << endl;
		//setZoneOptim(); // mniej optymalna funkcja
		//setZoneTuroEdition(); // mniej optymalna funkcja
		/// WYŚWIETLANIE WIZUALIZACJI 
		displayColors(); // drukuj kolorowe obszary na ekran - wyłącz w celu zwiększenia prędkości odświeżania

		/// PRZESŁANIE OBLICZONYCH KOLORÓW DO BUFFERA
		uint8_t * colors = RotateArray(AvgColors()); // pobierz array[3x98] RGB uśrednionych kolorów
		for (int i = 6; i < sizeof(buffer); i++) { // nadpisz bufor wartościami koloró z array
			buffer[i] = colors[i - 6]; // R
			buffer[i + 1] = colors[i - 6 + 1]; // G
			buffer[i + 2] = colors[i - 6 + 2]; // B
		}

		/// WYSYŁANIE BUFFORU DO ARDUINO
		arduino.writeSerialPortUC(buffer, MAX_DATA_LENGTH); // wyślij bufor na port usb
		al_flip_display(); // odśwież ekran
	}

	/// ZWOLNIJ PAMIĘĆ
	al_destroy_display(display);
	al_destroy_timer(timer);
	delete[] buffer;

	system("pause");
	return 0;
}