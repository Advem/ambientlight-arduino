function displayRGB() {

    var valueR = document.querySelector('input[name="R"]').value;
    var valueG = document.querySelector('input[name="G"]').value;
    var valueB = document.querySelector('input[name="B"]').value;
    var valueA = document.querySelector('input[name="A"]').value;

    var pR = document.querySelector('span[id="R"]');
    var pG = document.querySelector('span[id="G"]');
    var pB = document.querySelector('span[id="B"]');
    var pA = document.querySelector('span[id="A"]');

    pR.innerHTML = valueR;
    pG.innerHTML = valueG;
    pB.innerHTML = valueB;
    pA.innerHTML = valueA;

    document.body.style.background = `rgba(${valueR},${valueG},${valueB},${valueA/100})`;
}

setInterval(displayRGB, 10);